package todo;

import static todo.db.tables.TodoItem.TODO_ITEM;
import static todo.db.tables.TodoList.TODO_LIST;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import todo.db.tables.records.TodoListRecord;

@Transactional
@RestController
@RequestMapping("/api/v1/todoLists")
public class TodoListController {

    @Autowired DSLContext dsl;

    @RequestMapping(method=RequestMethod.GET, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<TodoListEntity> viewLists(final UserAuthentication principal) {
        return
            dsl.selectFrom(TODO_LIST)
                .where(TODO_LIST.OWNER_ID.eq(principal.getAppUser().getId()))
                .orderBy(TODO_LIST.NAME)
                .fetch()
                .into(TodoListEntity.class);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public TodoListEntity viewList(@PathVariable final Long id,
                                   final UserAuthentication principal) {
        final TodoListEntity list =
            dsl.selectFrom(TODO_LIST)
                .where(TODO_LIST.ID.eq(id))
                .and(TODO_LIST.OWNER_ID.eq(principal.getAppUser().getId()))
                .fetchOne()
                .into(TodoListEntity.class);
        list.setItems(
            dsl.selectFrom(TODO_ITEM)
                .where(TODO_ITEM.LIST_ID.eq(id))
                .orderBy(TODO_ITEM.TEXT)
                .fetchInto(TodoItemEntity.class));
        return list;
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PUT, 
                    consumes=MediaType.APPLICATION_JSON_VALUE, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public TodoListEntity updateList(@PathVariable final Long id, 
                                     @Valid @RequestBody final TodoListEntity list,
                                     final BindingResult result,
                                     final UserAuthentication principal) {
        if (result.hasErrors()) {
            throw new InvalidRequestException(result);
        }
        final TodoListRecord existingRecord =
            dsl.selectFrom(TODO_LIST)
                .where(TODO_LIST.ID.eq(id))
                .and(TODO_LIST.OWNER_ID.eq(principal.getAppUser().getId()))
                .orderBy(TODO_LIST.NAME)
                .forUpdate()
                .fetchAny();
        /* Not all fields are allowed to be updated here - not sure how to code
         * that without a long list of set() calls. */
        existingRecord.setName(list.getName());
        existingRecord.update();
        return existingRecord.into(TodoListEntity.class);
    }

    @RequestMapping(method=RequestMethod.POST, 
                    consumes=MediaType.APPLICATION_JSON_VALUE, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TodoListEntity> addList(@Valid @RequestBody final TodoListEntity list,
                                                  final BindingResult result,
                                                  final UserAuthentication principal) 
            throws Exception {
        if (result.hasErrors()) {
            throw new InvalidRequestException(result);
        }
        list.setId(null);
        final TodoListRecord listRecord = dsl.newRecord(TODO_LIST, list);
        listRecord.setOwnerId(principal.getAppUser().getId());
        listRecord.insert();
        return ResponseEntity
            .created(new URI("/api/v1/todoLists/" + listRecord.getId()))
            .body(listRecord.into(TodoListEntity.class));
    }

}
