package todo;

import static todo.db.tables.TodoItem.TODO_ITEM;
import static todo.db.tables.TodoList.TODO_LIST;

import java.net.URI;

import javax.validation.Valid;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import todo.db.tables.records.TodoItemRecord;
import todo.db.tables.records.TodoListRecord;

@Transactional
@RestController
@RequestMapping("/api/v1/todoItems")
public class TodoItemController {

    @Autowired DSLContext dsl;

    @RequestMapping(method=RequestMethod.POST, 
                    consumes=MediaType.APPLICATION_JSON_VALUE, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TodoItemEntity> addItem(@Valid @RequestBody final TodoItemEntity item,
                                                  final BindingResult result,
                                                  final UserAuthentication principal) 
            throws Exception {
        if (result.hasErrors()) {
            throw new InvalidRequestException(result);
        }
        final TodoListRecord list =
            dsl.selectFrom(TODO_LIST)
                .where(TODO_LIST.ID.eq(item.getListId()))
                .and(TODO_LIST.OWNER_ID.eq(principal.getAppUser().getId()))
                .fetchOne();
        item.setId(null);
        final TodoItemRecord itemRecord = dsl.newRecord(TODO_ITEM, item);
        itemRecord.setListId(list.getId());
        itemRecord.insert();
        return ResponseEntity
            .created(new URI("/api/v1/todoItems/" + itemRecord.getId()))
            .body(itemRecord.into(TodoItemEntity.class));
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PUT, 
                    consumes=MediaType.APPLICATION_JSON_VALUE, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public TodoItemEntity editItem(@PathVariable final Long id,
                                   @Valid @RequestBody final TodoItemEntity item,
                                   final BindingResult result,
                                   final UserAuthentication principal) 
            throws Exception {
        if (result.hasErrors()) {
            throw new InvalidRequestException(result);
        }
        final TodoItemRecord itemRecord = loadItem(id, principal);
        /* Not all fields are allowed to be updated here - not sure how to code
         * that without a long list of set() calls. */
        itemRecord.setReminder(item.getReminder());
        itemRecord.setText(item.getText());
        itemRecord.update();
        return itemRecord.into(TodoItemEntity.class);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PATCH, 
                    consumes=MediaType.APPLICATION_JSON_VALUE, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public TodoItemEntity updateDone(@PathVariable final Long id,
                                     @Valid @RequestBody final TodoItemDoneEntity done,
                                     final BindingResult result,
                                     final UserAuthentication principal) 
            throws Exception {
        if (result.hasErrors()) {
            throw new InvalidRequestException(result);
        }
        final TodoItemRecord itemRecord = loadItem(id, principal);
        itemRecord.setDone(done.isDone());
        itemRecord.update();
        return itemRecord.into(TodoItemEntity.class);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public void deleteItem(@PathVariable final Long id,
                           final UserAuthentication principal) 
            throws Exception {
        final TodoItemRecord itemRecord = loadItem(id, principal);
        itemRecord.delete();
    }

    private TodoItemRecord loadItem(final Long id, final UserAuthentication principal) {
        return 
            dsl.selectFrom(TODO_ITEM)
                .where(TODO_ITEM.ID.eq(id))
                .andExists(
                    dsl.selectOne()
                        .from(TODO_LIST)
                        .where(TODO_ITEM.LIST_ID.eq(TODO_LIST.ID))
                        .and(TODO_LIST.OWNER_ID.eq(principal.getAppUser().getId())))
                .fetchOne();
    }
}
