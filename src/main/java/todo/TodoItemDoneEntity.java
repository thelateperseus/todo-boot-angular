package todo;

import javax.validation.constraints.NotNull;

public class TodoItemDoneEntity {
    @NotNull
    private boolean done;
    
    public boolean isDone() {
        return done;
    }
    
    public void setDone(boolean done) {
        this.done = done;
    }
}