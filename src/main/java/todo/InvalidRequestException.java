package todo;

import org.springframework.validation.Errors;

public class InvalidRequestException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private Errors errors;

    public InvalidRequestException(Errors errors) {
        super(errors.toString());
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }
}
