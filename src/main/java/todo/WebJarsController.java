package todo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.RequireJS;

@RestController
public class WebJarsController {

    @RequestMapping(value="/webjarsjs", produces="application/javascript")
    public String webjarjs() {
        return RequireJS.getSetupJavaScript("/webjars/");
    }
}
