package todo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import todo.db.tables.pojos.AppUser;

public class TokenAuthenticationService {

    public static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";
    private static final long EXPIRY_MILLIS = 4 * 60 * 60 * 1000;

    private String secret;
    private UserService userService;

    public TokenAuthenticationService(String secret, UserService userService) {
        this.secret = secret;
        this.userService = userService;
    }

    public UserAuthentication getAuthentication(HttpServletRequest request) {
        final String token = request.getHeader(AUTH_HEADER_NAME);
        if (token != null) {
            final AppUser user = parseUserFromToken(token);
            if (user != null) {
                return new UserAuthentication(user);
            }
        }
        return null;
    }

    public AppUser parseUserFromToken(String token) {
        final Jws<Claims> claimsJws = 
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);

        SignatureAlgorithm algorithm = 
            SignatureAlgorithm.forName(claimsJws.getHeader().getAlgorithm());
        if (algorithm == SignatureAlgorithm.NONE) {
            throw new SignatureException("Invalid signature algorithm");
        }

        String username = claimsJws.getBody().getSubject();
        return userService.loadAppUserByUsername(username);
    }

    public String createTokenForUser(AppUser user) {
        return Jwts.builder()
            .setSubject(user.getEmail())
            .setIssuedAt(new Date())
            .setExpiration(new Date(System.currentTimeMillis() + EXPIRY_MILLIS))
            .signWith(SignatureAlgorithm.HS256, secret)
            .compact();
    }
}