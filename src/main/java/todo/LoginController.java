package todo;

import static todo.db.tables.AppUser.APP_USER;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.jooq.DSLContext;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import todo.db.tables.pojos.AppUser;
import todo.db.tables.records.AppUserRecord;

@Transactional
@RestController
@RequestMapping("/api/v1")
public class LoginController {

    @Autowired private DSLContext dsl;
    @Autowired private TokenAuthenticationService tokenService;

    @Value("${todo.signingKey}")
    private String signingKey;

    @RequestMapping(value="/login", method=RequestMethod.POST, 
                    consumes=MediaType.APPLICATION_JSON_VALUE, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> login(@Valid @RequestBody final UserLoginEntity login,
                                   final BindingResult result)  {
        if (result.hasErrors()) {
            throw new InvalidRequestException(result);
        }
        final AppUserRecord userRecord = 
            dsl.selectFrom(APP_USER)
                .where(APP_USER.EMAIL.eq(login.email))
                .fetchOne();
        if (userRecord == null || !BCrypt.checkpw(login.password, userRecord.getPasswordhash())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        final String token = tokenService.createTokenForUser(userRecord.into(AppUser.class));
        return ResponseEntity.ok()
            .header(TokenAuthenticationService.AUTH_HEADER_NAME, token)
            .body(userRecord.into(UserEntity.class));
    }

    @RequestMapping(value="/profile", method=RequestMethod.GET, 
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public UserEntity profile(final UserAuthentication principal) {
        final UserEntity user = new UserEntity();
        user.setEmail(principal.getAppUser().getEmail());
        user.setFullname(principal.getAppUser().getFullname());
        user.setId(principal.getAppUser().getId());
        return user;
    }

    private static class UserLoginEntity {
        @NotNull
        @Size(min=1, max=255)
        public String email;

        @NotNull
        @Size(min=1, max=255)
        public String password;
    }
}
