package todo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserEntity {

    private Long   id;
    private String email;
    private String fullname;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @NotNull
    @Size(min=1, max=255)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    @NotNull
    @Size(min=1, max=255)
    public String getFullname() {
        return this.fullname;
    }

    public void setFullname(final String fullname) {
        this.fullname = fullname;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserEntity(");

        sb.append(id);
        sb.append(", ").append(email);
        sb.append(", ").append(fullname);

        sb.append(")");
        return sb.toString();
    }
}
