package todo;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import todo.db.tables.pojos.AppUser;

public class UserAuthentication implements Authentication {

    private static final long serialVersionUID = 1L;

    private final AppUser user;
    private boolean authenticated = true;

    public UserAuthentication(AppUser user) {
        this.user = user;
    }

    @Override
    public String getName() {
        return user.getEmail();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public Object getCredentials() {
        return user.getPasswordhash();
    }

    @Override
    public AppUser getDetails() {
        return user;
    }
    
    public AppUser getAppUser() {
        return user;
    }

    @Override
    public Object getPrincipal() {
        return user.getEmail();
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
