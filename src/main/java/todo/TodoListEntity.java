package todo;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TodoListEntity {

    private Long id;
    private String name;
    private List<TodoItemEntity> items;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @NotNull
    @Size(min=1, max=255)
    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<TodoItemEntity> getItems() {
        return items;
    }
    
    public void setItems(final List<TodoItemEntity> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TodoListEntity(");

        sb.append(id);
        sb.append(", ").append(name);

        sb.append(")");
        return sb.toString();
    }

}
