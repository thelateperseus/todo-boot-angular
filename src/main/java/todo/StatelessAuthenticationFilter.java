package todo;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

public class StatelessAuthenticationFilter extends GenericFilterBean {

    private final TokenAuthenticationService authenticationService;

    public StatelessAuthenticationFilter(TokenAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        UserAuthentication authentication = null;
        try {
            authentication = authenticationService.getAuthentication(httpRequest);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        // Generate a new token if successfully authorised
        if (authentication != null) {
            httpResponse.setHeader(
                TokenAuthenticationService.AUTH_HEADER_NAME, 
                authenticationService.createTokenForUser(authentication.getAppUser()));
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
        
        SecurityContextHolder.getContext().setAuthentication(null);
    }
}