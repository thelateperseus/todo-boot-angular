package todo;

import static todo.db.tables.AppUser.APP_USER;

import java.util.Collections;

import org.jooq.DSLContext;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import todo.db.tables.pojos.AppUser;
import todo.db.tables.records.AppUserRecord;

public class UserService implements UserDetailsService {

    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

    private DSLContext dsl;
    
    public UserService(DSLContext dsl) {
        this.dsl = dsl;
    }
    
    @Override
    public final User loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = loadAppUserByUsername(username);
        User user = new User(appUser.getEmail(), appUser.getPasswordhash(), Collections.emptyList());
        detailsChecker.check(user);
        return user;
    }
    
    public final AppUser loadAppUserByUsername(String username) throws UsernameNotFoundException {
        AppUserRecord userRecord = 
            dsl.selectFrom(APP_USER)
                .where(APP_USER.EMAIL.eq(username))
                .fetchOne();
        if (userRecord == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return userRecord.into(AppUser.class);
    }
}