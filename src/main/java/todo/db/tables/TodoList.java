/**
 * This class is generated by jOOQ
 */
package todo.db.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;

import todo.db.DefaultSchema;
import todo.db.Keys;
import todo.db.tables.records.TodoListRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.2",
		"schema version:public_1"
	},
	date = "2016-02-18T00:16:10.689Z",
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TodoList extends TableImpl<TodoListRecord> {

	private static final long serialVersionUID = 1192629744;

	/**
	 * The reference instance of <code>todo_list</code>
	 */
	public static final TodoList TODO_LIST = new TodoList();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<TodoListRecord> getRecordType() {
		return TodoListRecord.class;
	}

	/**
	 * The column <code>todo_list.id</code>.
	 */
	public final TableField<TodoListRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>todo_list.name</code>.
	 */
	public final TableField<TodoListRecord, String> NAME = createField("name", org.jooq.impl.SQLDataType.VARCHAR.length(255).nullable(false), this, "");

	/**
	 * The column <code>todo_list.owner_id</code>.
	 */
	public final TableField<TodoListRecord, Long> OWNER_ID = createField("owner_id", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

	/**
	 * Create a <code>todo_list</code> table reference
	 */
	public TodoList() {
		this("todo_list", null);
	}

	/**
	 * Create an aliased <code>todo_list</code> table reference
	 */
	public TodoList(String alias) {
		this(alias, TODO_LIST);
	}

	private TodoList(String alias, Table<TodoListRecord> aliased) {
		this(alias, aliased, null);
	}

	private TodoList(String alias, Table<TodoListRecord> aliased, Field<?>[] parameters) {
		super(alias, DefaultSchema.DEFAULT_SCHEMA, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<TodoListRecord, Long> getIdentity() {
		return Keys.IDENTITY_TODO_LIST;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<TodoListRecord> getPrimaryKey() {
		return Keys.PK_TODO_LIST;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<TodoListRecord>> getKeys() {
		return Arrays.<UniqueKey<TodoListRecord>>asList(Keys.PK_TODO_LIST);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<TodoListRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<TodoListRecord, ?>>asList(Keys.TODO_LIST__FK_TODO_LIST_OWNER_2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TodoList as(String alias) {
		return new TodoList(alias, this);
	}

	/**
	 * Rename this table
	 */
	public TodoList rename(String name) {
		return new TodoList(name, null);
	}
}
