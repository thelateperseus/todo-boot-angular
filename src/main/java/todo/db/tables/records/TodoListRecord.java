/**
 * This class is generated by jOOQ
 */
package todo.db.tables.records;


import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;

import todo.db.tables.TodoList;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.2",
		"schema version:public_1"
	},
	date = "2016-02-18T00:16:10.689Z",
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TodoListRecord extends UpdatableRecordImpl<TodoListRecord> implements Record3<Long, String, Long> {

	private static final long serialVersionUID = -660004640;

	/**
	 * Setter for <code>todo_list.id</code>.
	 */
	public void setId(Long value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>todo_list.id</code>.
	 */
	@NotNull
	public Long getId() {
		return (Long) getValue(0);
	}

	/**
	 * Setter for <code>todo_list.name</code>.
	 */
	public void setName(String value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>todo_list.name</code>.
	 */
	@NotNull
	@Size(max = 255)
	public String getName() {
		return (String) getValue(1);
	}

	/**
	 * Setter for <code>todo_list.owner_id</code>.
	 */
	public void setOwnerId(Long value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>todo_list.owner_id</code>.
	 */
	@NotNull
	public Long getOwnerId() {
		return (Long) getValue(2);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Long> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record3 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row3<Long, String, Long> fieldsRow() {
		return (Row3) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row3<Long, String, Long> valuesRow() {
		return (Row3) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field1() {
		return TodoList.TODO_LIST.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field2() {
		return TodoList.TODO_LIST.NAME;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Long> field3() {
		return TodoList.TODO_LIST.OWNER_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value2() {
		return getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long value3() {
		return getOwnerId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TodoListRecord value1(Long value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TodoListRecord value2(String value) {
		setName(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TodoListRecord value3(Long value) {
		setOwnerId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TodoListRecord values(Long value1, String value2, Long value3) {
		value1(value1);
		value2(value2);
		value3(value3);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached TodoListRecord
	 */
	public TodoListRecord() {
		super(TodoList.TODO_LIST);
	}

	/**
	 * Create a detached, initialised TodoListRecord
	 */
	public TodoListRecord(Long id, String name, Long ownerId) {
		super(TodoList.TODO_LIST);

		setValue(0, id);
		setValue(1, name);
		setValue(2, ownerId);
	}
}
