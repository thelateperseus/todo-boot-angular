/**
 * This class is generated by jOOQ
 */
package todo.db;


import javax.annotation.Generated;

import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.UniqueKey;
import org.jooq.impl.AbstractKeys;

import todo.db.tables.AppUser;
import todo.db.tables.TodoItem;
import todo.db.tables.TodoList;
import todo.db.tables.records.AppUserRecord;
import todo.db.tables.records.TodoItemRecord;
import todo.db.tables.records.TodoListRecord;


/**
 * A class modelling foreign key relationships between tables of the <code></code> 
 * schema
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.2",
		"schema version:public_1"
	},
	date = "2016-02-18T00:16:10.689Z",
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

	// -------------------------------------------------------------------------
	// IDENTITY definitions
	// -------------------------------------------------------------------------

	public static final Identity<AppUserRecord, Long> IDENTITY_APP_USER = Identities0.IDENTITY_APP_USER;
	public static final Identity<TodoItemRecord, Long> IDENTITY_TODO_ITEM = Identities0.IDENTITY_TODO_ITEM;
	public static final Identity<TodoListRecord, Long> IDENTITY_TODO_LIST = Identities0.IDENTITY_TODO_LIST;

	// -------------------------------------------------------------------------
	// UNIQUE and PRIMARY KEY definitions
	// -------------------------------------------------------------------------

	public static final UniqueKey<AppUserRecord> PK_APP_USER = UniqueKeys0.PK_APP_USER;
	public static final UniqueKey<TodoItemRecord> PK_TODO_ITEM = UniqueKeys0.PK_TODO_ITEM;
	public static final UniqueKey<TodoListRecord> PK_TODO_LIST = UniqueKeys0.PK_TODO_LIST;

	// -------------------------------------------------------------------------
	// FOREIGN KEY definitions
	// -------------------------------------------------------------------------

	public static final ForeignKey<TodoItemRecord, TodoListRecord> TODO_ITEM__FK_TODO_ITEM_LIST_1 = ForeignKeys0.TODO_ITEM__FK_TODO_ITEM_LIST_1;
	public static final ForeignKey<TodoListRecord, AppUserRecord> TODO_LIST__FK_TODO_LIST_OWNER_2 = ForeignKeys0.TODO_LIST__FK_TODO_LIST_OWNER_2;

	// -------------------------------------------------------------------------
	// [#1459] distribute members to avoid static initialisers > 64kb
	// -------------------------------------------------------------------------

	private static class Identities0 extends AbstractKeys {
		public static Identity<AppUserRecord, Long> IDENTITY_APP_USER = createIdentity(AppUser.APP_USER, AppUser.APP_USER.ID);
		public static Identity<TodoItemRecord, Long> IDENTITY_TODO_ITEM = createIdentity(TodoItem.TODO_ITEM, TodoItem.TODO_ITEM.ID);
		public static Identity<TodoListRecord, Long> IDENTITY_TODO_LIST = createIdentity(TodoList.TODO_LIST, TodoList.TODO_LIST.ID);
	}

	private static class UniqueKeys0 extends AbstractKeys {
		public static final UniqueKey<AppUserRecord> PK_APP_USER = createUniqueKey(AppUser.APP_USER, AppUser.APP_USER.ID);
		public static final UniqueKey<TodoItemRecord> PK_TODO_ITEM = createUniqueKey(TodoItem.TODO_ITEM, TodoItem.TODO_ITEM.ID);
		public static final UniqueKey<TodoListRecord> PK_TODO_LIST = createUniqueKey(TodoList.TODO_LIST, TodoList.TODO_LIST.ID);
	}

	private static class ForeignKeys0 extends AbstractKeys {
		public static final ForeignKey<TodoItemRecord, TodoListRecord> TODO_ITEM__FK_TODO_ITEM_LIST_1 = createForeignKey(todo.db.Keys.PK_TODO_LIST, TodoItem.TODO_ITEM, TodoItem.TODO_ITEM.LIST_ID);
		public static final ForeignKey<TodoListRecord, AppUserRecord> TODO_LIST__FK_TODO_LIST_OWNER_2 = createForeignKey(todo.db.Keys.PK_APP_USER, TodoList.TODO_LIST, TodoList.TODO_LIST.OWNER_ID);
	}
}
