package todo;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TodoItemEntity {

    private Long      id;
    private Long      listId;
    private String    text;
    private Timestamp reminder;
    private Boolean   done;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }
    
    public Long getListId() {
        return listId;
    }
    
    public void setListId(final Long listId) {
        this.listId = listId;
    }

    @NotNull
    @Size(min=1, max=255)
    public String getText() {
        return this.text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public Timestamp getReminder() {
        return this.reminder;
    }

    public void setReminder(final Timestamp reminder) {
        this.reminder = reminder;
    }

    public Boolean getDone() {
        return this.done;
    }

    public void setDone(final Boolean done) {
        this.done = done;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TodoItem (");

        sb.append(id);
        sb.append(", ").append(text);
        sb.append(", ").append(reminder);
        sb.append(", ").append(done);

        sb.append(")");
        return sb.toString();
    }
}
