/*global define */

'use strict';

define(['angular'], function(angular) {

/* Services */

// Demonstrate how to register services
// In this case it is a simple value service.
var todoServices = angular.module('todoServices', ['ngResource']);

todoServices.factory('TodoList', ['$resource',
  function($resource){
    return $resource('/api/v1/todoLists/:listId', {}, {
      query: {method:'GET', isArray:true},
      update: {method:'PUT'}
    });
  }]);

todoServices.factory('TodoItem', ['$resource',
  function($resource){
    return $resource('/api/v1/todoItems/:itemId', {}, {
      query: {method:'GET', isArray:true},
      update: {method:'PUT'}
    });
  }]);

todoServices.config(['$httpProvider', function($httpProvider) {  
  if (sessionStorage.token) {
    $httpProvider.defaults.headers.common['X-Auth-Token'] = sessionStorage.token;
  }

  $httpProvider.interceptors.push(function() {
    return {
      response: function(response) {
        var token = response.headers('X-Auth-Token');
        if (token) {
          $httpProvider.defaults.headers.common['X-Auth-Token'] = token;
          sessionStorage.token = token;
        }
        return response;
      }
    }
  });
}]);

});
