/*global define */

'use strict';

define(['angular'], function(angular) {

  var todoControllers = angular.module('todoControllers', []);

  todoControllers.controller('HomeCtrl', ['$scope',
    function($scope) {
    }]);
  
  todoControllers.controller('ApplicationCtrl', ['$scope', '$state', '$http',
    function ($scope, $state, $http) {
      $scope.currentUser = null;

      // Check if the user is logged in when the app is loaded
      $http({ignoreAuthModule: true, method: 'GET', url: 'api/v1/profile'})
        .then(function(user) {
          if (user.data) {
            $scope.setCurrentUser(user.data);
          }
        });

      $scope.setCurrentUser = function(user) {
        $scope.currentUser = user;
      };
      
      $scope.logout = function() {
        $scope.setCurrentUser(null);
        delete $http.defaults.headers.common['X-Auth-Token'];
        sessionStorage.removeItem('token');
        $state.go('home');
      }
    }]);

  todoControllers.controller('LoginCtrl', ['$scope', '$http', 'authService',
    function ($scope, $http, authService) {
      $scope.loginDisplayed = false;
      $scope.credentials = {
        email: '',
        password: ''
      };

      $scope.login = function (credentials) {
        $scope.errorMessage = null;
        $http({ignoreAuthModule: true, method: 'POST', url: 'api/v1/login', data: credentials})
          .then(function(response) {
            authService.loginConfirmed(response.data);
            $scope.setCurrentUser(response.data);
          }, function(error) {
            $scope.errorMessage = "Invalid email or password";
          });
      };
    }]);

  todoControllers.controller('TodoListsCtrl', ['$scope', 'TodoList',
    function($scope, TodoList) {
      $scope.lists = TodoList.query();
    }]);

  todoControllers.controller('NewListCtrl', ['$scope', '$state', 'TodoList',
    function($scope, $state, TodoList) {
      $scope.name = "";
      $scope.errors = null;
      document.getElementById('name').focus();

      $scope.save = function() {
        var newList = new TodoList({name: $scope.name});
        $scope.errors = null;

        newList.$save(function(data) {
          // Add new list to left menu
          $scope.$parent.lists.push(data);
          $scope.errors = null;
        }, function(error) {
          $scope.errors = error.data.errors;
        });
      };
    }]);

  todoControllers.controller('ListDetailCtrl', ['$scope', '$state', '$stateParams', 'TodoList', 'TodoItem',
    function($scope, $state, $stateParams, TodoList, TodoItem) {
      $scope.list = TodoList.get({listId: $stateParams.listId});
      $scope.isAddingItem = false;
      $scope.datepickerOpened = false;
      
      $scope.openDatepicker = function($event) {
        $scope.datepickerOpened = true;
      };

      $scope.edit = function() {
        $scope.isEditing = true;
      };

      $scope.addItem = function() {
        $scope.newText = null;
        $scope.isAddingItem = true;
      };

      $scope.saveNewItem = function() {
        var newItem = new TodoItem({
          listId: $scope.list.id, text: $scope.newText, reminder: $scope.newReminder, done: false});
        $scope.errors = null;

        newItem.$save(function(data) {
          $scope.list.items.push(data);
          $scope.isAddingItem = false;
        }, function(error) {
          $scope.errors = error.data.errors;
        });
      };

      $scope.cancelAddItem = function() {
        $scope.isAddingItem = false;
      };

      $scope.acceptChanges = function() {
        if ($scope.isEditing) {
          $scope.errors = null;
          $scope.list.$update({listId: $stateParams.listId}, function(data) {
            // Update existing list in model
            $($scope.$parent.lists).each(function(index, list) {
              if (list.id === Number($stateParams.listId)) {
                list.name = $scope.list.name;
              }
            });
            $scope.isEditing = false;
          }, function(error) {
            $scope.errors = error.data.errors;
          });
        }
      };
    }]);

  todoControllers.controller('ItemCtrl', ['$scope', '$state', '$modal', '$http', 'TodoItem',
    function($scope, $state, $modal, $http, TodoItem) {
      $scope.isEditing = false;
      $scope.datepickerOpened = false;
      
      $scope.openDatepicker = function($event) {
        $scope.datepickerOpened = true;
      };

      $scope.editItem = function() {
        $scope.isEditing = true;
      };

      $scope.cancelEditing = function() {
        // TODO Revert edits somehow?
        $scope.isEditing = false;
      };

      $scope.saveItem = function() {
        $scope.errors = null;

        new TodoItem($scope.item).$update({itemId: $scope.item.id}, function(data) {
          $scope.isEditing = false;
          // Model is already updated
        }, function(error) {
          $scope.errors = error.data.errors;
        });
      };

      Object.defineProperty($scope, 'isDone', {
        get: function() {
          return $scope.item.done;
        },
        set: function(val) {
          if ( $scope.item.done !== val ) {
            $http.patch('/api/v1/todoItems/' + $scope.item.id, {done: val})
              .then(function(data) {
                $scope.item.done = val;
              }, function(error) {
                $scope.errors = error.data.errors;
                // TODO Handle errors
                console.log('Error setting done for item ' + $scope.item.id + ' ' + error.data);
              });
          }
        }
      });
      
      $scope.removeItem = function() {
        var modalInstance = $modal.open({
          animation: true,
          templateUrl: 'DeleteDialogContent.html',
          controller: 'DeleteDialogCtrl'
        });

        modalInstance.result.then(function() {
          TodoItem.delete({itemId: $scope.item.id}, function(data) {
            var items = $scope.$parent.list.items;
            items.splice(items.indexOf($scope.item), 1);
          }, function(error) {
            $scope.errors = error.data.errors;
            // TODO Handle errors
            console.log('Error deleting item ' + $scope.item.id + ' ' + error.data);
          });
        });
      };
    }]);

  todoControllers.controller('DeleteDialogCtrl', ['$scope', '$modalInstance',
    function ($scope, $modalInstance) {
      $scope.ok = function () {
        $modalInstance.close();
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    }]);
});