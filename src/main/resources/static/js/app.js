/*global require */

'use strict';

require(['angular', 
    './controllers', 
    './directives', 
    './filters', 
    './services', 
    'angular-route', 
    'angular-resource', 
    'angular-ui-router', 
    'ui-bootstrap', 
    'ui-bootstrap-tpls', 
    'angular-http-auth'],
  function(angular) {

    // Declare app level module which depends on filters, and services

    var todoApp = angular.module('todoApp', [
      'ui.router',
      'ui.bootstrap',
      'ui.bootstrap.tpls',
      'todoControllers', 
      'todoFilters', 
      'todoServices', 
      'todoDirectives', 
      'http-auth-interceptor']);

    todoApp.run(['$rootScope', '$state', '$stateParams',
      function ($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
      }]);

    todoApp.config(['$stateProvider', '$urlRouterProvider',
      function ($stateProvider, $urlRouterProvider) {
        // If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
        $urlRouterProvider.otherwise('/');

        $stateProvider
          .state("home", {
            url: '/',
            templateUrl: 'partials/home.html', 
            controller: 'HomeCtrl'})
          .state('todoLists', {
            url: '/todoLists',
            templateUrl: 'partials/todoLists.html', 
            controller: 'TodoListsCtrl'})
          .state('todoLists.new', {
            url: '/new',
            templateUrl: 'partials/newList.html', 
            controller: 'NewListCtrl'})
          .state('todoLists.detail', {
            url: '/{listId:[0-9]+}',
            templateUrl: 'partials/listDetail.html', 
            controller: 'ListDetailCtrl'});
      }]);

    angular.bootstrap(document, ['todoApp']);

});
